/*In The Name Of Allaah*/
/***************************** Task of this code *****************************/
/*	In this program we want to use a systamcall to find the time	     */
/*		the system.						     */
/*	To do this task, we use the timer systemcall of linux kernel	     */
/*	The number of this syscall is 201, so we use it to find the          */
/*		time of the system.					     */
/*	To get the time of the system in string format, we use a function    */
/*		is called ctime(). This function get the time of the         */
/*		system and return the time in string format and human 	     */
/*		readable.						     */
/*****************************************************************************/
#include <stdio.h>
#include <linux/kernel.h>
#include <sys/syscall.h>
#include <unistd.h>
#include <time.h>
int main(){
	system("");
	long int ret_status = syscall(201); // number of time syscall in linux kernel is 201
	if(!ret_status){
		printf("An Error has been occured");
	}
	else{
		printf("time is              :\t %ld\n", ret_status);
		printf("time of the system is:\t %s", ctime(&ret_status));	
	}
	return 0; 
}
