//In The Name Of Allaah
/************************************* CPU Usage ********************************************/
/* For get cpu usage, we can read a file that is /stat/proc
the usage of cpu is written in this file. 
it has 10 number, we obtain the sum of  8 number of these numbers to find the work pure work of cpu.
3rd number and 4th number are the time that cpu not used.
for calculate the percentage of usage of cpu we read the file two times with 1 second delay, then calculate the difference between the numbers and then calculate the CPU usage.
*/
/********************************************************************************************//
#include <stdio.h>
#include <stdlib.h>

int main(void)
{
    long double a[10], b[10], loadavg;
    FILE *fp;
    char dump[50];

    for(;;)
    {
        fp = fopen("/proc/stat","r");
        fscanf(fp,"%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",&a[0],&a[1],&a[2],&a[3], &a[4],&a[5],&a[6], &a[7],&a[8],&a[9]);
        fclose(fp);
        sleep(1);

        fp = fopen("/proc/stat","r");
        fscanf(fp,"%*s %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf %Lf",&b[0],&b[1],&b[2],&b[3], &b[4],&b[5],&b[6], &b[7],&b[8],&b[9]);
        fclose(fp);

        loadavg = ((b[0]+b[1]+b[4]+b[5]+b[6]+b[7]+b[8]+b[9]) - (a[0]+a[1]+a[4]+a[5]+a[6]+a[7]+a[8]+a[9])) / ((b[0]+b[1]+b[2]+b[3]b[4]+b[5]+b[6]+b[7]+b[8]+b[9]) - (a[0]+a[1]+a[2]+a[3]+a[4]+a[5]+a[6]+a[7]+a[8]+a[9]));
        printf("CPU Usage : %Lf\n",loadavg * 100);
    }

    return(0);
}
